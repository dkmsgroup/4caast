package gr.ntua._4caast.priceaggegator.service.client;

import gr.ntua._4caast.priceaggegator.service.utils.ReadFile;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author idezol
 */
public class TestClient {
    
    public static void main( String[] args )
    {
        try {
            
            String result = ReadFile.readFromURL("http://147.102.19.176/4caast/prices.xml");
            System.out.println(result);
            
            //System.out.println(PriceAggregatorClient.doTestPut());
        } catch (IOException ex) {
            Logger.getLogger(TestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
