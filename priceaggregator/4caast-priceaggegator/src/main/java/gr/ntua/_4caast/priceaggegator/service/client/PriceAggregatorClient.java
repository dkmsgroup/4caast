package gr.ntua._4caast.priceaggegator.service.client;

import gr.ntua._4caast.priceaggegator.service.utils.ReadFile;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ws.rs.core.MediaType;

public class PriceAggregatorClient {
    
    private static final Logger Log = LoggerFactory.getLogger(PriceAggregatorClient.class);
    
    public static String doTest() {
        
        HttpURLConnection conn = null;
        String result = ""; 
       
        try {
            
            URL url = new URL("http://localhost:8080/4caast-priceaggregator/resources/priceaggregator");
            
            conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Accept", MediaType.TEXT_HTML);
            
            if(conn.getResponseCode() != 200 ) {
                //throw new RuntimeException("Failed. Http Code: " + conn.getResponseCode());
                System.out.println("Failed. Http Code: " + conn.getResponseCode());
            }
            
            BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output = "";
            result = "";
            System.out.println("Getting Server response ....");
            while((output = bf.readLine())!= null) {   
                result+=output;
            }
           
            
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("IOException: " + ex.getMessage());
        } finally {
            if(conn != null) 
                conn.disconnect();
        }
        
        return result;
    }

    public static String doTestPut() throws FileNotFoundException {
        
        String input = ReadFile.readFromFile("src/main/resources/blueprint.xml");
        return _doTestPut(input);
    }
    
    private static String _doTestPut(String inputParameter) {
        
        
        HttpURLConnection conn = null;
        String result = ""; 
       
                
        try {
            
            URL url = new URL("http://localhost:8080/4caast-priceaggregator/resources/priceaggregator");
            
            conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod("PUT");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Accept", MediaType.TEXT_PLAIN);
            
            
            OutputStream out = conn.getOutputStream();
            Writer writer = new OutputStreamWriter(out, "UTF-8");
            inputParameter = "blueprintlist=" + URLEncoder.encode(inputParameter, "UTF-8").replace("+", "%20");
            System.out.println("input: " + inputParameter);
            
            writer.write(inputParameter);
            writer.close();
            out.close();
            
            if(conn.getResponseCode() != 200 ) {
                //throw new RuntimeException("Failed. Http Code: " + conn.getResponseCode());
                System.out.println("Failed. Http Code: " + conn.getResponseCode());
            }
            
            BufferedReader bf = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String output = "";
            result = "";
            System.out.println("Getting Server response ....");
            while((output = bf.readLine())!= null) {   
                result+=output;
            }
           
            
        } catch (MalformedURLException ex) {
            System.out.println("MalformedURLException: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("IOException: " + ex.getMessage());
        } finally {
            if(conn != null) 
                conn.disconnect();
        }
        
        
        
        
        return result;
    }

}
