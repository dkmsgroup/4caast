package gr.ntua._4caast.priceaggegator.service.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 *
 * @author idezol
 */
public class ReadFile {
    
    public static String readFromFile(String filename) throws FileNotFoundException {
        
        StringBuilder fileData = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        char[] buf = new char[1024];
        int numRead=0;
        try {
            while((numRead=reader.read(buf)) != -1){
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
        } catch(IOException ex) {
            System.out.println("IOException: " + ex.getMessage());
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                //DO NOTHING
            }
        }
        
        return fileData.toString();
    }
    
    public static String readFromURL(String url) throws IOException  {
        
        URL oracle = new URL(url);
        StringBuilder builder = new StringBuilder();
        BufferedReader in = new BufferedReader(
        new InputStreamReader(oracle.openStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null)
            builder.append(inputLine);
        in.close();
        
        return builder.toString();
    }
    
    private static int readLines(String filename) throws FileNotFoundException, IOException {
        
        BufferedReader br = new BufferedReader(new FileReader(filename));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            int count = 0;
            while (line != null) {   
                System.out.println((++count) + " " + line);
                line = br.readLine();
            }
            
        } finally {
            br.close();
        }
        
        return 0;
    }
    
}
