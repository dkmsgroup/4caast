package gr.ntua._4caast.priceaggegator.service;

import gr.ntua._4caast.priceaggegator.service.utils.ReadFile;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST Web Service
 *
 * @author idezol
 */
@Path("priceaggregator")
public class PriceAggregator {
    
    private static final Logger Log = LoggerFactory.getLogger(PriceAggregator.class);
    

    @Context
    private UriInfo context;

    /** Creates a new instance of PriceAggregator */
    public PriceAggregator() {
    }

    /**
     * Retrieves representation of an instance of gr.ntua._4caast.priceaggegator.service.PriceAggregator
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String getXml() {
        Log.info(("Service is running"));
        return "<b>Hello world</b>";
    }
    
    /*
    @GET
    @Consumes(MediaType.TEXT_XML)
    @Produces(MediaType.TEXT_XML)
    public String getXml(@FormParam("blueprintlist") String blueprintList) {
        
        Log.info(("Received a list of bluepring"));
        return "<xml>Hello world</xml>";
    }
    */
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public String getXmlad(@FormParam("blueprintlist") String blueprintList) {
        
        Log.info("Received a list of bluepring");
        try {
            blueprintList = URLDecoder.decode(blueprintList, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Log.error(ex.getMessage());
        }
        
        String result = "";
        try {
            result = ReadFile.readFromURL("http://147.102.19.176/4caast/prices.xml");
        } catch (IOException ex) {
            Log.error(ex.getMessage());
        }
        
        return result;
    }
    
    

   
}
